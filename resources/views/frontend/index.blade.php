@extends('frontend.layouts.app')

@section('title', __('My Notes'))

@section('content')
    <notes-index user='{{ Auth::user()->id }}'></notes-index>
@endsection
