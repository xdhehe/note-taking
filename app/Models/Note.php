<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;

    protected $fillable = [
        'content',
        'is_shareable',
        'created_by',
        'updated_by'
    ];

    public function createdby()
    {
        return $this->belongsTo('App\Domains\Auth\Models\User', 'created_by');
    }

    public function updatedby()
    {
        return $this->belongsTo('App\Domains\Auth\Models\User', 'updated_by');
    }

}
