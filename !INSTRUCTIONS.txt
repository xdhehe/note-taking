go to terminal
> git clone https://gitlab.com/xdhehe/note-taking.git

go to phpmyadmin
create database name  "notes" or any

go to file explorer
rename ".env.example" file to .env

open .env file
setup .env change your .env variables, depend on your local setup needs specially these variables:

    APP_URL=http://localhost:8000

    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=notes
    DB_USERNAME=root
    DB_PASSWORD=

*PS* in ENV variable facebook and google keys are personally generated values

go to terminal
> composer install
> npm install
> npm run dev OR npm run watch

> php artisan migrate:fresh

> php artisan key:generate
> php artisan passport:keys

> php artisan optimize && php artisan optimize:clear

> php artisan serve

----------------------------------

Google Working App domain :
    localhost:8000
    localhost

FACEBOOK Working App domain : localhost
Test User:
    username: open_fjmxfoj_user@tfbnw.net
    pw: yay123456