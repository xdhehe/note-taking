<?php

use Illuminate\Http\Request;
use App\Http\Controllers\API\NotesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
// });

// GET|HEAD   | api/notes         | notes.index    | App\Http\Controllers\API\NotesController@index
// POST       | api/notes         | notes.store    | App\Http\Controllers\API\NotesController@store
// PUT|PATCH  | api/notes/{note}  | notes.update   | App\Http\Controllers\API\NotesController@update
// DELETE     | api/notes/{note}  | notes.destroy  | App\Http\Controllers\API\NotesController@destroy

Route::middleware('api')->group(function(){
    Route::resource('notes', NotesController::class)->only([
        'index', 'store', 'update','destroy'
    ]);
});