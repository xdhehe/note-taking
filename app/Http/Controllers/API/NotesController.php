<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Models\Note;
use Illuminate\Http\Request;
use App\Http\Requests\Frontend\Note\StoreNoteRequest;
use App\Http\Requests\Frontend\Note\UpdateNoteRequest;

use Auth;
use App\Models\Auth\User;

class NotesController extends Controller
{
    public function index(){
        if(! Auth::guard('api')->check()){ return response()->json([ "message" => "Unauthorized" ], 403); }
        $user = auth('api')->user();

        $notes = Note::where('is_shareable',1)->orWhere('created_by', $user->id)->with('createdby:id,name','updatedby:id,name')->orderBy('updated_at','desc')->get();
        return response()->json($notes, 200, array(), JSON_PRETTY_PRINT);
    }

    public function store(StoreNoteRequest $request)
    {
        if(! Auth::guard('api')->check()){ return response()->json([ "message" => "Unauthorized" ], 403); }
        $user = auth('api')->user();

        $note = new Note;
        $note->content = $request['data']['note'];
        $note->is_shareable = $request['data']['share'];
        $note->created_by = $user->id;
        $note->save();

        return response()->json([ "message" => "Created Successfully" ], 201);
    }

    public function update(UpdateNoteRequest $request, Note $note)
    {
        if(! Auth::guard('api')->check()){ return response()->json([ "message" => "Unauthorized" ], 403); }

        $user = auth('api')->user();
        $note->content = $request['note'];
        $note->updated_by = $user->id;
        $note->save();

        return response()->json([ "message" => "Updated Successfully" ], 200);
    }

    public function destroy(Note $note){
        if(! Auth::guard('api')->check()){ return response()->json([ "message" => "Unauthorized" ], 403); }
        $user = auth('api')->user();
        if($note->created_by == $user->id){
            $note->delete();
            return response()->json([ "message" => "Deleted Successfully"], 200);
        }
        else{
            return response()->json([ "message" => "Unauthorized" ], 403);
        }
    }
}